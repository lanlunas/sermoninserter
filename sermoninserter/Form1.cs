﻿using System;
using System.Windows.Forms;

namespace SermonInserter
{
    /// <summary>
    /// GUI that help inserting a sermon info to HTML code
    /// </summary>
    public partial class Form1 : Form
    {
        /// The string represents the start of the sermon list
        private const string StartText = "playList: [";

        /// The string represents the end of the sermon list
        private const string EndText = "] });";

        public Form1()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Loop through all TextBoxes of the form to check if all are filled in.
        /// </summary>
        /// <returns>
        /// A boolean with value True if all TextBox are filled in
        /// or false if there is any TextBox empty
        /// </returns>
        private bool CheckAllTextBoxesFilledIn()
        {
            foreach (Control c in this.Controls)
            {
                Console.WriteLine(c.Name);
                Console.WriteLine(c.GetType().ToString());
                if ((c.GetType().ToString() == "System.Windows.Forms.TextBox") && (c.Text == string.Empty))
                {
                    MessageBox.Show("All fields need to be filled in before continue!!!", "WARNING:");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check link in textAWSLink TextBox is to an audio file from  church's amazon s3
        /// </summary>
        /// <returns>A Boolean with value True if the link correct or false if not.</returns>
        private bool CheckLink()
        {
            if (!textAWSLink.Text.Contains("https://vietchurchnmsermons.s3.amazonaws.com/") ||
                !textAWSLink.Text.Contains(".mp3"))
            {
                var msg = "The AWS link entered is\r\n" + textAWSLink.Text +
                    "\r\nwhich is NOT in correct format.\r\nPlease check!!!";
                MessageBox.Show(msg, "Warning");
                return false;
            }

            return true;
        }

        /// <summary>Check if the passed date is Sunday.</summary>
        /// <param name="date">The DateTime to be checked.</param>
        /// <returns>A boolean with value True if date is Sunday or false if not.</returns>
        private bool CheckSunday(DateTime date)
        {
            var day = date.DayOfWeek;
            if (day != DayOfWeek.Sunday)
            {
                var msg = "The selected day is " + day +
                    "\r\nPlease click 'OK' to continue or 'Cancel' to go back " +
                    "to select another day.";
                var confirmResult = MessageBox.Show(
                    msg,
                    "WARNING: Selected day is NOT SUN DAY",
                    MessageBoxButtons.OKCancel);
                if (confirmResult == DialogResult.Cancel)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>Look for last position of markText in htmlText.</summary>
        /// <param name="markText">The string of which position need to be looked for.</param>
        /// <param name="htmlText">The string in which markText's position need to be looked for.</param>
        /// <returns>
        /// An integer represent for the position of markText in htmlText or -1 if not found
        /// </returns>
        private int GetPositionInHTML(string markText, string htmlText)
        {
            var markTextPos = htmlText.LastIndexOf(markText);
            if (markTextPos == -1)
            {
                var msg = "Can't find '" + markText + "' in the HTML code.\r\n" +
                    "Please check if you insert the correct code.";
                MessageBox.Show(msg, "Warning");
            }

            return markTextPos;
        }

        /// <summary>
        /// Reset the list of sermon in wixHTML by deleting 
        /// from the start of the list (pos1) to the end of the list.
        /// </summary>
        /// <param name="wixHTML">The string of wix's HTML.</param>
        /// <param name="pos1">The integer to be the start index of the list.</param>
        /// <param name="endText">The string from that the end list position can be found</param>
        /// <returns>A string representing the wixHTML with the sermon list has been removed.</returns>
        private string ResetSermonList(string wixHTML, int pos1, string endText)
        {
            // 6: 4 for indent, 2 for newline
            int pos2 = this.GetPositionInHTML(endText, wixHTML) - 6;
            string part1 = wixHTML.Substring(0, pos1);
            string part2 = wixHTML.Substring(pos2, wixHTML.Length - pos2);
            return part1 + part2;
        }

        /// <summary> Returns the sermon's info string.</summary>
        /// <param name="dateStr">/The string of the selected date in Vietnamese format</param>
        /// <param name="sermonName">The name of the sermon.</param>
        /// <param name="awsLink">The link of the audio file on aws/s3.</param>
        /// <returns>A string representing the sermon's info.</returns>
        private string GetSermonStr(string dateStr, string sermonName, string awsLink)
        {
            string sermonStr = "\r\n    {\r\n      'icon': iconImage,\r\n      " +
                "'title': '" + dateStr + " - " + sermonName +
                "',\r\n      'file': '" + awsLink + "' },";
            return sermonStr;
        }

        /// <summary>
        /// Collect the entered info
        /// Reset sermon list in wixHTML if required
        /// Add new sermon to wixHTML
        /// Add new wixHTML to clipboard.
        /// </summary>
        private void Process_Click(object sender, EventArgs e)
        {
            var date = calendar.SelectionRange.Start;
            if (!this.CheckAllTextBoxesFilledIn() || !this.CheckSunday(date) || !this.CheckLink())
            {
                return;
            }

            var dateStr = date.ToString("dd/MM/yyyy");
            var wixHTML = textHTML.Text;
            int pos1 = this.GetPositionInHTML(StartText, wixHTML) + StartText.Length;
            
            if (checkBoxReset.Checked)
            {
                wixHTML = this.ResetSermonList(wixHTML, pos1, EndText);
            }

            string sermonStr = this.GetSermonStr(dateStr, textSermonName.Text, textAWSLink.Text);
            wixHTML = wixHTML.Insert(pos1, sermonStr);
            //// add new HTML code to clipboard
            Clipboard.SetText(wixHTML);

            var successfulMsg = "The new HTML code is in the Clipboard." +
                "\r\nPlease paste it into the Edit Code box of WIX's audio widget.";
            MessageBox.Show(successfulMsg, "Successfully Process");
        }

        /// <summary>
        /// Close the app
        /// </summary>
        private void Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Clear textHTML
        /// </summary>
        private void ClearHTML_Click(object sender, EventArgs e)
        {
            textHTML.Clear();
        }
    }
}
