﻿namespace SermonInserter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.calendar = new System.Windows.Forms.MonthCalendar();
            this.label1 = new System.Windows.Forms.Label();
            this.textSermonName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textAWSLink = new System.Windows.Forms.TextBox();
            this.textHTML = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonProcess = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.checkBoxReset = new System.Windows.Forms.CheckBox();
            this.buttonClearHTML = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // calendar
            // 
            this.calendar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.calendar.Location = new System.Drawing.Point(318, 5);
            this.calendar.MaxSelectionCount = 1;
            this.calendar.Name = "calendar";
            this.calendar.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Sermon Name";
            // 
            // textSermonName
            // 
            this.textSermonName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textSermonName.Location = new System.Drawing.Point(6, 31);
            this.textSermonName.Name = "textSermonName";
            this.textSermonName.Size = new System.Drawing.Size(304, 20);
            this.textSermonName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "AWS Link";
            // 
            // textAWSLink
            // 
            this.textAWSLink.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textAWSLink.Location = new System.Drawing.Point(6, 84);
            this.textAWSLink.Name = "textAWSLink";
            this.textAWSLink.Size = new System.Drawing.Size(304, 20);
            this.textAWSLink.TabIndex = 4;
            // 
            // textHTML
            // 
            this.textHTML.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textHTML.Location = new System.Drawing.Point(6, 178);
            this.textHTML.MaxLength = 200000;
            this.textHTML.Multiline = true;
            this.textHTML.Name = "textHTML";
            this.textHTML.Size = new System.Drawing.Size(538, 192);
            this.textHTML.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "HTML";
            // 
            // buttonProcess
            // 
            this.buttonProcess.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonProcess.Location = new System.Drawing.Point(184, 394);
            this.buttonProcess.Name = "buttonProcess";
            this.buttonProcess.Size = new System.Drawing.Size(185, 28);
            this.buttonProcess.TabIndex = 7;
            this.buttonProcess.Text = "PROCESS";
            this.buttonProcess.UseVisualStyleBackColor = true;
            this.buttonProcess.Click += new System.EventHandler(this.Process_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClose.Location = new System.Drawing.Point(387, 394);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(157, 27);
            this.buttonClose.TabIndex = 8;
            this.buttonClose.Text = "CLOSE";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.Close_Click);
            // 
            // checkBoxReset
            // 
            this.checkBoxReset.AutoSize = true;
            this.checkBoxReset.Location = new System.Drawing.Point(6, 128);
            this.checkBoxReset.Name = "checkBoxReset";
            this.checkBoxReset.Size = new System.Drawing.Size(112, 17);
            this.checkBoxReset.TabIndex = 9;
            this.checkBoxReset.Text = "Reset Sermon List";
            this.checkBoxReset.UseVisualStyleBackColor = true;
            // 
            // buttonClearHTML
            // 
            this.buttonClearHTML.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonClearHTML.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClearHTML.Location = new System.Drawing.Point(16, 394);
            this.buttonClearHTML.Name = "buttonClearHTML";
            this.buttonClearHTML.Size = new System.Drawing.Size(151, 28);
            this.buttonClearHTML.TabIndex = 10;
            this.buttonClearHTML.Text = "Clear HTML";
            this.buttonClearHTML.UseVisualStyleBackColor = true;
            this.buttonClearHTML.Click += new System.EventHandler(this.ClearHTML_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 433);
            this.Controls.Add(this.buttonClearHTML);
            this.Controls.Add(this.checkBoxReset);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonProcess);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textHTML);
            this.Controls.Add(this.textAWSLink);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textSermonName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.calendar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Sermon Inserter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar calendar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textSermonName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textAWSLink;
        private System.Windows.Forms.TextBox textHTML;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonProcess;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.CheckBox checkBoxReset;
        private System.Windows.Forms.Button buttonClearHTML;
    }
}

