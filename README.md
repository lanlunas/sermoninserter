# README #

This project is to simplify the task to add new sermon into audio widget's HTML in VN ABQ church's website.

### Information needed ###

* Sermon's name
* The date the sermon is given
* The link to the sermon's audio on Amazon S3
* The HTML code for the audio widget on church website

### How to run? ###

* Click on Process button: 
	* The new sermon info will be created in the correct format 
	* The above info will be inserted into the correct position in the given HTML code.
	* The new HTML code will be set in clipboard, ready to be pasted to replace the old HTML code on the website.

### Extra features ###

* Clear HTML: Clear the textbox for HTML
* Reset Sermon List: If this check box is checked, all the list of sermons will be removed before new sermon infor is inserted.

### Who do I talk to? ###

* Leave an issue on this repo if you catch any bug or have any suggestion to improve the app.